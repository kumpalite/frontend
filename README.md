# kumpalite
Check [this](https://kumpalite.netlify.com) out

## Requirements

For development, you will only need Node.js installed on your environement.

## Install

    $ git clone https://gitlab.com/kumpalite/frontend.git
    $ cd frontend
    $ yarn
    or
    $ npm install

### Configure app

Create `.env` on project root with this following fields:

```sh
AUTH_URL=YOUR_AUTH_URL
GRAPHQL_URL=YOUR_GRAPHQL_URL
```

## Start & watch

    $ yarn dev
    or
    $ npm run dev

## Simple build for production

    $ yarn build
    or
    $ npm run build

---

## Languages & tools


- [Ant Design](https://ant.design/)
- [React](https://reactjs.org/)
- [React Apollo](https://www.apollographql.com/docs/react/)
- [Slate](https://www.slatejs.org/)
- [Styled Component](https://www.styled-components.com/)

## Author

- [Ganesha Danu Enastika](https://gitlab.com/BlinfoldKing)
- [Igar Ramaddhan](https://gitlab.com/igarramaddhan)
- [Luthfi Nur Aida](https://gitlab.com/na.luthfi)