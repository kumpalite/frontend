// @flow
import {useContext} from 'react';
import TokenContext from '../contexts/TokenContext';

const useToken = () => {
  const ctx = useContext(TokenContext);
  return ctx;
};

export default useToken;
