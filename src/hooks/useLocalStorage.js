import {useState} from 'react';
const useLocalStorage = (key, initialValue) => {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? item : initialValue;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      return initialValue;
    }
  });

  const setValue = value => {
    try {
      const valueToStore =
        value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);

      if (value) {
        window.localStorage.setItem(
          key,
          typeof valueToStore === 'string'
            ? valueToStore
            : JSON.stringify(valueToStore)
        );
      } else {
        window.localStorage.removeItem(key);
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  };

  return [storedValue, setValue];
};

export default useLocalStorage;
