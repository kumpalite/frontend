const stories = [
  {
    id: 1,
    title: 'Test Story 1',
    user: {name: 'Test User'},
    created_at: new Date(),
  },
  {
    id: 2,
    title: 'Test Story 2',
    user: {name: 'Test User'},
    created_at: new Date(),
  },
  {
    id: 3,
    title: 'Test Story 3',
    user: {name: 'Test User'},
    created_at: new Date(),
  },
  {
    id: 4,
    title: 'Test Story 4',
    user: {name: 'Test User'},
    created_at: new Date(),
  },
  {
    id: 5,
    title: 'Test Story 5',
    user: {name: 'Test User'},
    created_at: new Date(),
  },
  {
    id: 6,
    title: 'Test Story 6',
    user: {name: 'Test User'},
    created_at: new Date(),
  },
];

export {stories};
