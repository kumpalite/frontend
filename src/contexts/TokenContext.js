// @flow
import {createContext} from 'react';

type TokenContextType = {
  token: ?string,
  setToken: (token: ?string) => void,
};

const TokenContext = createContext<TokenContextType>({
  token: null,
  setToken: () => {},
});

export default TokenContext;
