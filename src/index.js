import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './style/main.scss';
import Provider from './graphql/Provider';
import TokenContext from './contexts/TokenContext';
import useLocalStorage from './hooks/useLocalStorage';
import GlobalErrorBoundary from './components/GlobalErrorBoundary';

const Main = () => {
  const [token, setToken] = useLocalStorage('token', null);
  return (
    <GlobalErrorBoundary>
      <TokenContext.Provider value={{token, setToken}}>
        <Provider>
          <App />
        </Provider>
      </TokenContext.Provider>
    </GlobalErrorBoundary>
  );
};

ReactDOM.render(<Main />, document.getElementById('root'));
