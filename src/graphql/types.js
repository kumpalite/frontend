/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateArticle
// ====================================================

export type CreateArticle_CreateArticle_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type CreateArticle_CreateArticle = {|
  __typename: "Article",
  id: string,
  slug: string,
  title: string,
  createdAt: string,
  updatedAt: string,
  author: ?CreateArticle_CreateArticle_author,
  thumbnail: string,
  totalComment: number,
|};

export type CreateArticle = {|
  CreateArticle: ?CreateArticle_CreateArticle
|};

export type CreateArticleVariables = {|
  slug: string,
  title: string,
  status: string,
  content: string,
  category: Array<?string>,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetAllArticle
// ====================================================

export type GetAllArticle_GetArticles_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type GetAllArticle_GetArticles = {|
  __typename: "Article",
  id: string,
  slug: string,
  title: string,
  createdAt: string,
  updatedAt: string,
  author: ?GetAllArticle_GetArticles_author,
  thumbnail: string,
  totalComment: number,
|};

export type GetAllArticle = {|
  GetArticles: ?Array<?GetAllArticle_GetArticles>
|};

export type GetAllArticleVariables = {|
  userId?: ?string,
  keyword?: ?string,
  username?: ?string,
  category?: ?string,
  status?: ?string,
  offset?: ?number,
  limit?: ?number,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetArticle
// ====================================================

export type GetArticle_GetArticleBySlug_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type GetArticle_GetArticleBySlug = {|
  __typename: "Article",
  id: string,
  slug: string,
  title: string,
  createdAt: string,
  updatedAt: string,
  content: string,
  category: Array<?string>,
  author: ?GetArticle_GetArticleBySlug_author,
  totalComment: number,
|};

export type GetArticle = {|
  GetArticleBySlug: ?GetArticle_GetArticleBySlug
|};

export type GetArticleVariables = {|
  slug: string
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetCommentByArticleSlug
// ====================================================

export type GetCommentByArticleSlug_GetCommentByArticleSlug_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type GetCommentByArticleSlug_GetCommentByArticleSlug = {|
  __typename: "Comment",
  id: string,
  content: string,
  author: ?GetCommentByArticleSlug_GetCommentByArticleSlug_author,
|};

export type GetCommentByArticleSlug = {|
  GetCommentByArticleSlug: ?Array<?GetCommentByArticleSlug_GetCommentByArticleSlug>
|};

export type GetCommentByArticleSlugVariables = {|
  slug: string,
  limit?: ?number,
  offset?: ?number,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateComment
// ====================================================

export type CreateComment_CreateComment_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type CreateComment_CreateComment = {|
  __typename: "Comment",
  id: string,
  content: string,
  author: ?CreateComment_CreateComment_author,
|};

export type CreateComment = {|
  CreateComment: ?CreateComment_CreateComment
|};

export type CreateCommentVariables = {|
  content: string,
  idArticle: string,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetUser
// ====================================================

export type GetUser_GetUser = {|
  __typename: "User",
  email: string,
  id: string,
  fullname: string,
  username: string,
|};

export type GetUser = {|
  GetUser: ?GetUser_GetUser
|};

export type GetUserVariables = {|
  id?: ?string,
  username?: ?string,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetRelatedArticles
// ====================================================

export type GetRelatedArticles_GetRelatedArticles_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type GetRelatedArticles_GetRelatedArticles = {|
  __typename: "Article",
  id: string,
  slug: string,
  title: string,
  createdAt: string,
  updatedAt: string,
  author: ?GetRelatedArticles_GetRelatedArticles_author,
  thumbnail: string,
  totalComment: number,
|};

export type GetRelatedArticles = {|
  GetRelatedArticles: ?Array<?GetRelatedArticles_GetRelatedArticles>
|};

export type GetRelatedArticlesVariables = {|
  slug: string
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateArticle
// ====================================================

export type UpdateArticle_UpdateArticle_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type UpdateArticle_UpdateArticle = {|
  __typename: "Article",
  id: string,
  slug: string,
  title: string,
  createdAt: string,
  updatedAt: string,
  author: ?UpdateArticle_UpdateArticle_author,
|};

export type UpdateArticle = {|
  UpdateArticle: ?UpdateArticle_UpdateArticle
|};

export type UpdateArticleVariables = {|
  id: string,
  slug: string,
  title: string,
  status: string,
  content: string,
  category: Array<?string>,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetUserCategory
// ====================================================

export type GetUserCategory_GetCategoriesByUserId = {|
  __typename: "Category",
  name: string,
  slugCategory: string,
|};

export type GetUserCategory = {|
  GetCategoriesByUserId: ?Array<?GetUserCategory_GetCategoriesByUserId>
|};

export type GetUserCategoryVariables = {|
  idAuthor: string,
  offset?: ?number,
  limit?: ?number,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateCategory
// ====================================================

export type CreateCategory_CreateCategory = {|
  __typename: "Category",
  name: string,
  slugCategory: string,
|};

export type CreateCategory = {|
  CreateCategory: ?CreateCategory_CreateCategory
|};

export type CreateCategoryVariables = {|
  name: string,
  slugCategory: string,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: baseUserFragment
// ====================================================

export type baseUserFragment = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: baseArticleFragment
// ====================================================

export type baseArticleFragment = {|
  __typename: "Article",
  id: string,
  slug: string,
  title: string,
  createdAt: string,
  updatedAt: string,
|};
/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: baseCommentFragment
// ====================================================

export type baseCommentFragment_author = {|
  __typename: "User",
  id: string,
  fullname: string,
  username: string,
|};

export type baseCommentFragment = {|
  __typename: "Comment",
  id: string,
  content: string,
  author: ?baseCommentFragment_author,
|};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================