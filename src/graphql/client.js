// @flow
import React from 'react';
import {render} from 'react-dom';
import {HttpLink} from 'apollo-link-http';
import {ApolloClient} from 'apollo-client';
import {InMemoryCache, defaultDataIdFromObject} from 'apollo-cache-inmemory';
import {setContext} from 'apollo-link-context';
import {ApolloLink} from 'apollo-link';
import {onError} from 'apollo-link-error';
import ErrorComponent from '../components/ErrorComponent';

const cache = new InMemoryCache({
  dataIdFromObject: object => {
    switch (object.__typename) {
      case 'User':
        return `user:${object.username}`;
      case 'Article':
        return `article:${object.id}`;
      case 'Comment':
        return `comment:${object.id}`;
      case 'Category':
        return `category:${object.slugCategory}`;
      default:
        return defaultDataIdFromObject(object);
    }
  },
});

const url: string = (process.env.GRAPHQL_URL: any);

// eslint-disable-next-line consistent-return
const errorLink = onError(({graphQLErrors, networkError}) => {
  if (graphQLErrors)
    graphQLErrors.map(({message, locations, path}) => {
      if (!locations) return message;
      const gqlErrorMessage = `[GraphQL error]: Message: ${message}, Location: ${locations.reduce(
        (prev, current) =>
          `${prev} ${JSON.stringify(current)}`
            .replace(/[{"}]/g, '')
            .replace(/[,:]/g, ' '),
        ''
      )}, Path: ${path}`;
      // eslint-disable-next-line no-console
      console.log(gqlErrorMessage);
      return render(
        <ErrorComponent message={gqlErrorMessage} />,
        // $FlowFixMe
        document.getElementById('root')
      );
    });
  if (networkError) {
    const networkErrorMessage = `[Network error]: ${networkError}`;
    // eslint-disable-next-line no-console
    console.log(networkErrorMessage);
    return render(
      <ErrorComponent message={networkErrorMessage} />,
      // $FlowFixMe
      document.getElementById('root')
    );
  }
});

const authLink = setContext((_, {headers}) => {
  const token = localStorage.getItem('token');
  const otherHeaders = {};
  if (token) {
    otherHeaders.Authorization = token;
  }
  return {
    headers: {
      ...headers,
      ...otherHeaders,
    },
  };
});

const httpLink = new HttpLink({
  uri: url,
});

const client = new ApolloClient({
  cache,
  link: ApolloLink.from([errorLink, authLink, httpLink]),
  // link: httpLink,
  resolvers: {},
});

export default client;
