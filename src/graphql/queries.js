import gql from 'graphql-tag';

const BASE_USER_FRAGMENT = gql`
  fragment baseUserFragment on User {
    id
    fullname
    username
  }
`;

const BASE_ARTICLE_FRAGMENT = gql`
  fragment baseArticleFragment on Article {
    id
    slug
    title
    createdAt
    updatedAt
  }
`;

export const BASE_COMMENT_FRAGMENT = gql`
  fragment baseCommentFragment on Comment {
    id
    content
    author {
      ...baseUserFragment
    }
  }
  ${BASE_USER_FRAGMENT}
`;

export const CREATE_ARTICLE = gql`
  mutation CreateArticle(
    $slug: String!
    $title: String!
    $status: String!
    $content: String!
    $category: [String]!
  ) {
    CreateArticle(
      article: {
        slug: $slug
        title: $title
        status: $status
        content: $content
        category: $category
      }
    ) {
      ...baseArticleFragment
      author {
        ...baseUserFragment
      }
      thumbnail
      totalComment
    }
  }
  ${BASE_USER_FRAGMENT}
  ${BASE_ARTICLE_FRAGMENT}
`;

export const GET_ALL_ARTICLE = gql`
  query GetAllArticle(
    $userId: ID
    $keyword: String
    $username: String
    $category: String
    $status: String = "PUBLISHED"
    $offset: Int = 0
    $limit: Int = 5
  ) {
    GetArticles(
      userId: $userId
      status: $status
      keyword: $keyword
      username: $username
      category: $category
      offset: $offset
      limit: $limit
    ) {
      ...baseArticleFragment
      author {
        ...baseUserFragment
      }
      thumbnail
      totalComment
    }
  }
  ${BASE_USER_FRAGMENT}
  ${BASE_ARTICLE_FRAGMENT}
`;

export const GET_ARTICLE = gql`
  query GetArticle($slug: String!) {
    GetArticleBySlug(slug: $slug) {
      ...baseArticleFragment
      content
      category
      author {
        ...baseUserFragment
      }
      totalComment
    }
  }
  ${BASE_USER_FRAGMENT}
  ${BASE_ARTICLE_FRAGMENT}
`;

export const GET_ARTICLE_COMMENTS = gql`
  query GetCommentByArticleSlug(
    $slug: String!
    $limit: Int = 5
    $offset: Int = 0
  ) {
    GetCommentByArticleSlug(slug: $slug, limit: $limit, offset: $offset) {
      ...baseCommentFragment
    }
  }
  ${BASE_COMMENT_FRAGMENT}
`;

export const CREATE_COMMENT = gql`
  mutation CreateComment($content: String!, $idArticle: ID!) {
    CreateComment(comment: {content: $content, idArticle: $idArticle}) {
      ...baseCommentFragment
    }
  }
  ${BASE_COMMENT_FRAGMENT}
`;

export const GET_USER = gql`
  query GetUser($id: ID, $username: String) {
    GetUser(id: $id, username: $username) {
      email
      ...baseUserFragment
    }
  }
  ${BASE_USER_FRAGMENT}
`;

export const GET_RELATED_ARTICLES = gql`
  query GetRelatedArticles($slug: String!) {
    GetRelatedArticles(slug: $slug) {
      ...baseArticleFragment
      author {
        ...baseUserFragment
      }
      thumbnail
      totalComment
    }
  }
  ${BASE_USER_FRAGMENT}
  ${BASE_ARTICLE_FRAGMENT}
`;

export const UPDATE_ARTICLE = gql`
  mutation UpdateArticle(
    $id: ID!
    $slug: String!
    $title: String!
    $status: String!
    $content: String!
    $category: [String]!
  ) {
    UpdateArticle(
      article: {
        slug: $slug
        title: $title
        idArticle: $id
        status: $status
        content: $content
        category: $category
      }
    ) {
      ...baseArticleFragment
      author {
        ...baseUserFragment
      }
    }
  }
  ${BASE_USER_FRAGMENT}
  ${BASE_ARTICLE_FRAGMENT}
`;

export const GET_USER_CATEGORY = gql`
  query GetUserCategory($idAuthor: ID!, $offset: Int = 0, $limit: Int = 5) {
    GetCategoriesByUserId(idAuthor: $idAuthor, offset: $offset, limit: $limit) {
      name
      slugCategory
    }
  }
`;

export const CREATE_CATEGORY = gql`
  mutation CreateCategory($name: String!, $slugCategory: String!) {
    CreateCategory(category: {name: $name, slugCategory: $slugCategory}) {
      name
      slugCategory
    }
  }
`;
