// @flow
import React, {Fragment} from 'react';
import styled from 'styled-components';
import {Value} from 'slate';
import {Editor as SlateEditor} from 'slate-react';
// import {Bold, Italic, List, Code, Underline, Image} from '';

import {
  renderMark,
  renderBlock,
  MarkHotkey,
  onKeyDown,
  insertImage,
} from './EditorUtils';

import FormatToolbar from '../FormatToolbar';
import ToolTipButton from '../TooltipButton';
import {
  Icon,
  Affix,
  Modal,
  Input,
  Divider,
  Button,
  Upload,
  message,
} from 'antd';

// Create our initial value...
const initialValue = Value.fromJSON({
  document: {
    nodes: [
      {
        object: 'block',
        type: 'paragraph',
        nodes: [
          {
            object: 'text',
            text: '',
          },
        ],
      },
    ],
  },
});

// Create an array of plugins.
const plugins = [
  MarkHotkey({key: 'b', type: 'bold'}),
  MarkHotkey({key: 'l', type: 'list'}),
  MarkHotkey({key: '`', type: 'code'}),
  MarkHotkey({key: "'", type: 'quote'}),
  MarkHotkey({key: 'i', type: 'italic'}),
  MarkHotkey({key: 'u', type: 'underline'}),
  MarkHotkey({key: '-', type: 'strikethrough'}),
];

const UploadImageContainer = styled.div`
  text-align: center;
`;

const StyledEditor = styled(SlateEditor)`
  min-height: 56px;
  line-height: 1.58;
  font-size: 1.1em;

  blockquote {
    color: grey;
    border-left: 3px #ff9900 solid;
    padding-left: 8px;
    margin-left: 16px;
    font-style: italic;
  }

  pre {
    background: #e5e5e5;
    padding: 8px;

    & > code {
      padding: 0;
      font-size: inherit;
      color: black;
    }
  }

  @media only screen and (max-width: 768px) {
    blockquote {
      margin: 16px 0;
    }
  }
`;

const schema = {
  blocks: {
    image: {
      isVoid: true,
    },
  },
};

type Props = {
  value: Value,
  isEnableEditing?: boolean,
  onEditorValueChange: (value: Value) => void,
};

type State = {
  value: Value,
  modalVisible: boolean,
};

// const getValueString = (value: Value) => JSON.stringify(value.toJSON());

class Editor extends React.Component<Props, State> {
  editor: SlateEditor;
  uploadInput: Input;
  static defaultProps = {
    isEnableEditing: true,
    onEditorValueChange: () => {},
  };

  static getDerivedStateFromProps(props: Props) {
    const obj = {
      value: props.value,
    };

    if (obj.value) {
      return obj;
    }
    return null;
  }

  state = {
    value: initialValue,
    modalVisible: false,
  };

  onChange = ({value}: {value: Value}) => {
    this.props.onEditorValueChange(value);
  };

  onMarkClick = (e: Event, type: string) => {
    e.preventDefault();
    if (type !== 'list' && !type.includes('heading')) {
      const change = this.editor.toggleMark(type);
      this.onChange(change);
    } else {
      this.editor.setBlocks(type);
    }
  };

  insert = (src: string) => {
    if (!src) return;
    this.editor.command(insertImage, src);
    this.editor.insertBlock('paragraph');
  };

  handleModal = async () => {
    await this.insert(this.uploadInput.state.value);
    this.setState({modalVisible: false});
    this.uploadInput.setState({value: ''});
  };

  onUploadChange = async (info: any) => {
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
      const image = info.file.response.path;
      await this.insert(image);
      this.setState({modalVisible: false});
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  };

  render() {
    const {modalVisible} = this.state;
    return (
      <Fragment>
        {this.props.isEnableEditing && (
          <Affix offsetTop={0}>
            <FormatToolbar>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'bold')}>
                <Icon type="bold" />
              </ToolTipButton>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'italic')}>
                <Icon type="italic" />
              </ToolTipButton>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'list')}>
                <Icon type="unordered-list" />
              </ToolTipButton>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'code')}>
                <code style={{fontSize: 14}}>code</code>
              </ToolTipButton>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'underline')}>
                <Icon type="underline" />
              </ToolTipButton>
              <ToolTipButton
                onClick={e => this.onMarkClick(e, 'strikethrough')}>
                <Icon type="strikethrough" />
              </ToolTipButton>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'heading-one')}>
                <span style={{fontWeight: 'bold'}}>H1</span>
              </ToolTipButton>
              <ToolTipButton onClick={e => this.onMarkClick(e, 'heading-two')}>
                <span style={{fontWeight: 'bold'}}>H2</span>
              </ToolTipButton>
              <ToolTipButton
                onClick={e => this.onMarkClick(e, 'heading-three')}>
                <span style={{fontWeight: 'bold'}}>H3</span>
              </ToolTipButton>
              <ToolTipButton
                onClick={() => this.setState({modalVisible: true})}>
                <Icon type="file-image" />
              </ToolTipButton>
            </FormatToolbar>
          </Affix>
        )}

        <Modal
          title="Insert Image"
          onOk={this.handleModal}
          visible={modalVisible}
          onCancel={() => this.setState({modalVisible: false})}>
          <UploadImageContainer>
            <Upload
              name="file"
              showUploadList={false}
              onChange={this.onUploadChange}
              // $FlowFixMe
              action={`${process.env.AUTH_URL}file/upload`}>
              <Button>
                <Icon type="upload" /> Click to Upload
              </Button>
            </Upload>
            <Divider>or</Divider>
            <Input
              placeholder="Input url"
              ref={ref => (this.uploadInput = ref)}
            />
          </UploadImageContainer>
        </Modal>

        <StyledEditor
          ref={ref => (this.editor = ref)}
          placeholder="Write something"
          value={this.state.value}
          onChange={this.onChange}
          plugins={plugins}
          schema={schema}
          renderMark={renderMark}
          renderBlock={renderBlock}
          onKeyDown={onKeyDown}
          readOnly={!this.props.isEnableEditing}
        />
      </Fragment>
    );
  }
}

export default Editor;
