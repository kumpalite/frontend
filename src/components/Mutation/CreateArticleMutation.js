// @flow
import {Mutation} from 'react-apollo';
import {CREATE_ARTICLE} from '../../graphql/queries';
import {
  type CreateArticle,
  type CreateArticleVariables,
} from '../../graphql/types';

class CreateArticleMutation extends Mutation<
  CreateArticle,
  CreateArticleVariables
> {
  static mutation = CREATE_ARTICLE;
}

export default CreateArticleMutation;
