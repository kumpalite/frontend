// @flow
import {Mutation} from 'react-apollo';
import {UPDATE_ARTICLE} from '../../graphql/queries';
import {
  type UpdateArticle,
  type UpdateArticleVariables,
} from '../../graphql/types';

class UpdateArticleMutation extends Mutation<
  UpdateArticle,
  UpdateArticleVariables
> {
  static mutation = UPDATE_ARTICLE;
}

export default UpdateArticleMutation;
