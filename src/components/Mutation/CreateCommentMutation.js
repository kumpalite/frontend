// @flow
import {Mutation} from 'react-apollo';
import {CREATE_COMMENT} from '../../graphql/queries';
import type {CreateComment, CreateCommentVariables} from '../../graphql/types';

class CreateCommentMutation extends Mutation<
  CreateComment,
  CreateCommentVariables
> {
  static mutation = CREATE_COMMENT;
}

export default CreateCommentMutation;
