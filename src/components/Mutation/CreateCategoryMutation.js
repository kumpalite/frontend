// @flow
import {Mutation} from 'react-apollo';
import {CREATE_CATEGORY} from '../../graphql/queries';
import {
  type CreateCategory,
  type CreateCategoryVariables,
} from '../../graphql/types';

class CreateCategoryMutation extends Mutation<
  CreateCategory,
  CreateCategoryVariables
> {
  static mutation = CREATE_CATEGORY;
}

export default CreateCategoryMutation;
