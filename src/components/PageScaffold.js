// @flow
import React from 'react';
import {Layout} from 'antd';
import styled from 'styled-components';
import NavBar from './NavBar';

const {Content, Footer} = Layout;

const PageScaffoldContainer = styled.div`
  flex: 1;
  display: flex;
  padding-top: 56px;
  flex-direction: column;
`;

const FooterContent = styled.div`
  text-align: center;
`;

type Props = {
  // $FlowFixMe
  children: React.ReactNode,
};

const PageScaffold = (props: Props) => (
  <Layout>
    <PageScaffoldContainer>
      <NavBar />
      <Content>{props.children}</Content>
      <Footer>
        <FooterContent>kumpalite</FooterContent>
      </Footer>
    </PageScaffoldContainer>
  </Layout>
);

export default PageScaffold;
