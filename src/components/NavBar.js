// @flow
import React from 'react';
import {Button, message, Input, Modal, Icon} from 'antd';
import styled from 'styled-components';
import {Link, withRouter, RouteComponentProps} from 'react-router-dom';
import useToken from '../hooks/useToken';
import {ApolloConsumer} from 'react-apollo';

const {Search} = Input;

const HeaderContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  z-index: 10;
  height: 56px;
  display: flex;
  position: fixed;
  flex-direction: row;
  justify-content: center;
  background-color: white;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05), 0 3px 6px rgba(0, 0, 0, 0.05);
`;

const HeaderContentContainer = styled.div`
  display: flex;
  min-width: 80%;
  align-items: center;
`;

const SideContent = styled.div`
  flex: 0.6;
  display: flex;
  padding: 0 8px;
  align-items: center;
`;

const CenterContent = styled.div`
  flex-grow: 1;
`;

const LogoTitle = styled(Link)`
  font-size: 18px;
`;

type Props = {} & RouteComponentProps;

const NavBar = (props: Props) => {
  const {token, setToken} = useToken();

  return (
    <HeaderContainer>
      <HeaderContentContainer>
        <SideContent>
          <LogoTitle to="/">kumpalite</LogoTitle>
        </SideContent>
        <CenterContent>
          <Search
            placeholder="Type to search"
            onSearch={value => {
              // eslint-disable-next-line no-console
              if (value !== '') props.history.push(`/search/${value}`);
            }}
            style={{height: 40}}
          />
        </CenterContent>
        <SideContent style={{justifyContent: 'flex-end'}}>
          {token && props.location.pathname !== '/dashboard' && (
            <Button
              size="large"
              style={{marginRight: 8}}
              onClick={() => props.history.push('/dashboard')}>
              Dashboard
            </Button>
          )}
          <ApolloConsumer>
            {({resetStore}) => (
              <Button
                type={token ? 'danger' : 'link'}
                size="large"
                onClick={() => {
                  if (token) {
                    Modal.confirm({
                      title: 'Confirm log out',
                      content: 'Are you sure want to log out?',
                      okText: 'Yes',
                      okType: 'danger',
                      cancelText: 'Cancel',
                      icon: <Icon type="exclamation-circle" />,
                      onOk: () => {
                        setToken(null);
                        resetStore();
                        message.success('Log Out successfully', 2);
                      },
                    });
                  } else {
                    props.history.push('/login');
                  }
                }}>
                {token ? 'Log Out' : 'Log In'}
              </Button>
            )}
          </ApolloConsumer>
        </SideContent>
      </HeaderContentContainer>
    </HeaderContainer>
  );
};

export default withRouter(NavBar);
