// @flow
import React, {Fragment} from 'react';
import {Skeleton} from 'antd';
import Card from './Card';

const ListItem = () => (
  <Card>
    <Skeleton active paragraph={{rows: 2}} />
  </Card>
);

const ArticleListLoader = () => (
  <Fragment>
    <ListItem />
    <ListItem />
    <ListItem />
  </Fragment>
);

export default ArticleListLoader;
