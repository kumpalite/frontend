// @flow
import React from 'react';
import styled from 'styled-components';
import {Layout} from 'antd';

const LoaderContainer = styled.div`
  flex: 1;
  display: flex;
  min-height: 500px;
  align-items: center;
  justify-content: center;
`;

const PageLoader = () => (
  <Layout>
    <LoaderContainer>Loading</LoaderContainer>
  </Layout>
);

export default PageLoader;
