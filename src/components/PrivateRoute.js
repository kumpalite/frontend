// @flow
import React from 'react';
import {Route, Redirect, RouteProps} from 'react-router-dom';
import useToken from '../hooks/useToken';

const PrivateRoute = ({component: Component, render, ...rest}: RouteProps) => {
  const {token} = useToken();

  return (
    <Route
      {...rest}
      render={props =>
        token ? (
          Component ? (
            <Component {...props} />
          ) : (
            render(props)
          )
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: {from: props.location},
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
