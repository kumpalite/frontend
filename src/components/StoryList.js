// @flow
import React from 'react';
import StoryCard from './StoryCard';
import {type GetAllArticle_GetArticles} from '../graphql/types';
import {List} from 'antd';
import LoadMoreButton from './LoadMoreButton';

type Props = {
  loading: boolean,
  withLoadMore?: boolean,
  onLoadMore?: Function,
  stories: Array<?GetAllArticle_GetArticles>,
};

const StoryList = ({withLoadMore = false, ...props}: Props) => (
  <React.Fragment>
    <List
      loading={props.loading}
      loadMore={
        withLoadMore && props.stories.length > 0 ? (
          <LoadMoreButton
            loading={props.loading}
            onLoadMore={props.onLoadMore}
          />
        ) : null
      }
      dataSource={props.stories}
      renderItem={(story: GetAllArticle_GetArticles) => (
        <StoryCard key={story.id} story={story} />
      )}
    />
  </React.Fragment>
);

export default StoryList;
