// @flow
import {Query} from 'react-apollo';
import {GET_RELATED_ARTICLES} from '../../graphql/queries';
import {
  type GetRelatedArticles,
  type GetRelatedArticlesVariables,
} from '../../graphql/types';

class RelatedArticlesQuery extends Query<
  GetRelatedArticles,
  GetRelatedArticlesVariables
> {
  static query = GET_RELATED_ARTICLES;
}

export default RelatedArticlesQuery;
