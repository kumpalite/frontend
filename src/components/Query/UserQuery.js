// @flow
import {Query} from 'react-apollo';
import {GET_USER} from '../../graphql/queries';
import {type GetUser, type GetUserVariables} from '../../graphql/types';

class UserQuery extends Query<GetUser, GetUserVariables> {
  static query = GET_USER;
}

export default UserQuery;
