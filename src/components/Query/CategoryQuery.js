// @flow
import {Query} from 'react-apollo';
import {GET_USER_CATEGORY} from '../../graphql/queries';
import type {
  GetUserCategory,
  GetUserCategoryVariables,
} from '../../graphql/types';

class CategoryQuery extends Query<GetUserCategory, GetUserCategoryVariables> {
  static query = GET_USER_CATEGORY;
}

export default CategoryQuery;
