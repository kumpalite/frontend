// @flow
import {Query} from 'react-apollo';
import {GET_ARTICLE_COMMENTS} from '../../graphql/queries';
import {
  type GetCommentByArticleSlug,
  type GetCommentByArticleSlugVariables,
} from '../../graphql/types';

class CommentsQuery extends Query<
  GetCommentByArticleSlug,
  GetCommentByArticleSlugVariables
> {
  static query = GET_ARTICLE_COMMENTS;
}

export default CommentsQuery;
