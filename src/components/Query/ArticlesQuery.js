// @flow
import {Query} from 'react-apollo';
import {GET_ALL_ARTICLE} from '../../graphql/queries';
import type {GetAllArticle, GetAllArticleVariables} from '../../graphql/types';

class ArticlesQuery extends Query<GetAllArticle, GetAllArticleVariables> {
  static query = GET_ALL_ARTICLE;
}

export default ArticlesQuery;
