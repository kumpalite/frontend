// @flow
import {Query} from 'react-apollo';
import {GET_ARTICLE} from '../../graphql/queries';
import type {GetArticle, GetArticleVariables} from '../../graphql/types';

class ArticleQuery extends Query<GetArticle, GetArticleVariables> {
  static query = GET_ARTICLE;
}

export default ArticleQuery;
