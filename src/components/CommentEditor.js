// @flow
import React from 'react';
import styled from 'styled-components';
import {Button, Input} from 'antd';

const {TextArea} = Input;

const CommentEditorContainer = styled.div`
  display: flex;
  margin-top: 16px;
`;

const ButtonContainer = styled.div`
  display: flex;
  margin-left: 8px;
  justify-content: flex-end;
`;

type CommentEditorProps = {
  value: string,
  disabled: boolean,
  submitting: boolean,
  onSubmit: () => void,
  // $FlowFixMe
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void,
};

const CommentEditor = ({
  value,
  onSubmit,
  onChange,
  disabled = false,
  submitting,
}: CommentEditorProps) => (
  <CommentEditorContainer>
    <TextArea
      rows={2}
      value={value}
      disabled={disabled}
      onChange={onChange}
      placeholder="Type comment..."
    />
    <ButtonContainer>
      <Button
        type="primary"
        htmlType="submit"
        onClick={onSubmit}
        disabled={disabled}
        loading={submitting}>
        Send
      </Button>
    </ButtonContainer>
  </CommentEditorContainer>
);

export default CommentEditor;
