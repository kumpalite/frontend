// @flow
import React from 'react';
import {Button} from 'antd';

type Props = {
  loading: boolean,
  onLoadMore: Function,
};

const LoadMoreButton = ({loading, onLoadMore}: Props) => (
  <div
    style={{
      textAlign: 'center',
      marginTop: 12,
      height: 32,
      lineHeight: '32px',
    }}>
    <Button loading={loading} onMouseDown={onLoadMore}>
      More
    </Button>
  </div>
);

export default LoadMoreButton;
