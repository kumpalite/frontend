// @flow
import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {timeDifference} from '../utils/dateConverter';
import {type GetAllArticle_GetArticles} from '../graphql/types';

import Card from './Card';
import {Icon, Divider} from 'antd';
import getValueFromObject from '../utils/getValueFromObject';

const StoryCardContainer = styled.div`
  display: flex;
`;

const StoryCardContent = styled.div`
  flex: 1;
`;

const Title = styled.h2`
  margin: 4px 0;
  a {
    color: black;
    &:hover {
      color: #40a9ff;
    }
  }
`;

const Author = styled.p``;

const Image = styled.img`
  width: 100px;
  height: 100px;
  object-fit: cover;
  margin: 13px 0 13px;
`;

type IconTextProps = {
  type: string,
  text: string,
};

const IconText = ({type, text}: IconTextProps) => (
  <span style={{position: 'relative'}}>
    <Icon type={type} style={{marginRight: 4, fontSize: 16}} />
    <span
      style={{
        top: -8,
        right: 2,
        fontSize: 11,
        borderRadius: 5,
        background: 'white',
        position: 'absolute',
      }}>
      {text}
    </span>
  </span>
);

type Props = {
  story: GetAllArticle_GetArticles,
};

const StoryCard = (props: Props) => {
  const story = props.story;
  return (
    <Card>
      <StoryCardContainer>
        <StoryCardContent>
          <Title>
            <Link to={`/story/${story.slug}`}>{story.title}</Link>
          </Title>
          <Author>
            <Link
              style={{color: 'grey'}}
              to={`/profile/${getValueFromObject(story, 'author.username')}`}>
              {story.author && story.author.fullname}
            </Link>
          </Author>
          <span>
            <IconText type="message" text={`${story.totalComment}`} />
            <Divider type="vertical" />
            {timeDifference(story.createdAt)}
          </span>
        </StoryCardContent>
        <Image alt={story.title} src={story.thumbnail} />
      </StoryCardContainer>
    </Card>
  );
};

export default StoryCard;
