// @flow
import React from 'react';
import {Helmet} from 'react-helmet';

type Props = {
  title: string,
  description: string,
};

const Meta = ({title, description}: Props) => (
  <Helmet>
    <title>{title}</title>
    <meta charSet="utf-8" />
    <meta name="description" content={description} />
    <link rel="canonical" href={window.location.href} />
  </Helmet>
);

export default Meta;
