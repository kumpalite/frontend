// @flow
import React from 'react';
import styled from 'styled-components';
import {Button} from 'antd';

const ErrorContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;

type Props = {
  message: string,
};

const ErrorComponent = (props: Props) => (
  <ErrorContainer>
    {props.message}
    <Button style={{marginTop: 16}} onClick={() => location.reload()}>
      Reload
    </Button>
  </ErrorContainer>
);

export default ErrorComponent;
