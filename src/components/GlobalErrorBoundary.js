// @flow
import React from 'react';
import ErrorComponent from './ErrorComponent';

type Props = {
  // $FlowFixMe
  children?: React.ReactNode,
};

type State = {hasError: boolean};

class GlobalErrorBoundary extends React.Component<Props, State> {
  static getDerivedStateFromError() {
    return {hasError: true};
  }

  state: State = {
    hasError: false,
  };

  componentDidCatch(error: any, info: any) {
    // Customized error handling goes here!
    // eslint-disable-next-line no-console
    console.log(error, info);
  }

  render() {
    if (this.state.hasError) {
      return <ErrorComponent message="Oops, something wrong here..." />;
    }

    return this.props.children;
  }
}

export default GlobalErrorBoundary;
