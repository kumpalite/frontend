import 'antd/dist/antd.css';
import React, {lazy, Suspense} from 'react';
import {hot} from 'react-hot-loader/root';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
// import pages here
const Home = lazy(() => import('./pages/Home'));
const Login = lazy(() => import('./pages/Login'));
const Register = lazy(() => import('./pages/Register'));
const Write = lazy(() => import('./pages/Write'));
const Story = lazy(() => import('./pages/Story'));
const Profile = lazy(() => import('./pages/Profile'));
const Search = lazy(() => import('./pages/Search'));
const Dashboard = lazy(() => import('./pages/Dashboard'));

import Meta from './components/Meta.js';
import PageLoader from './components/PageLoader.js';
import PrivateRoute from './components/PrivateRoute.js';
import PageScaffold from './components/PageScaffold.js';

function App() {
  return (
    <BrowserRouter>
      <Meta title="kumpalite" description="kumpalite App" />
      <PageScaffold>
        <Suspense fallback={<PageLoader />}>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <PrivateRoute exact path="/write/:slug" component={Write} />
            <PrivateRoute exact path="/dashboard" component={Dashboard} />
            <Route exact path="/story/:slug" component={Story} />
            <Route exact path="/search/:search" component={Search} />
            <Route path="/profile/:username" component={Profile} />
            <Route path="/" component={Home} />
          </Switch>
        </Suspense>
      </PageScaffold>
    </BrowserRouter>
  );
}

export default hot(App);
