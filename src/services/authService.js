// @flow
import Axios, {AxiosResponse} from 'axios';

type ResponseType = AxiosResponse<{token: string}>;

const login = async (identifier: string, password: string) => {
  const url: string = (process.env.AUTH_URL: any);
  const res: ResponseType = await Axios.post(`${url}login`, {
    identifier,
    password,
  });
  return res.data.token;
};

type RegisterProps = {
  username: string,
  email: string,
  fullname: string,
  password: string,
};

const register = async (props: RegisterProps) => {
  const url: string = (process.env.AUTH_URL: any);
  const res: ResponseType = await Axios.post(`${url}register`, props);
  return res.data.token;
};

export {login, register};
