// @flow
import React from 'react';
import styled from 'styled-components';
import {Layout, Typography} from 'antd';

import LoginForm from './LoginForm';

const {Title, Text} = Typography;

const Header = styled.div`
  text-align: center;
  padding: 10vh 0px 6vh;
`;

const TitleContainer = styled.div`
  text-align: center;
  margin: 0px auto 24px;
`;

const FormContainer = styled.div`
  width: 400px;
  padding: 16px;
  padding: 24px 40px;
  border-radius: 4px;
  margin: 0px auto 24px;
  background-color: white;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 10px;
`;

const Login = () => (
  <Layout>
    <Header>
      <Title>kumpalite</Title>
    </Header>
    <FormContainer>
      <TitleContainer>
        <Text style={{fontSize: 24}}>Log in to continue</Text>
      </TitleContainer>
      <LoginForm />
    </FormContainer>
  </Layout>
);

export default Login;
