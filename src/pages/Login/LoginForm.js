// @flow
import React, {useState} from 'react';
import styled from 'styled-components';
import {FormComponentProps} from 'antd/lib/form';
import {Form, Input, Icon, Button, message, Divider} from 'antd';
import {Link, RouteComponentProps, withRouter} from 'react-router-dom';
import {AxiosError} from 'axios';

import {login} from '../../services/authService';
import useToken from '../../hooks/useToken';

const RegisterLinkContainer = styled.div`
  margin-top: -12px;
  text-align: center;
`;

type Props = {} & FormComponentProps & RouteComponentProps;

const LoginFormBase = (props: Props) => {
  const {setToken} = useToken();
  const [loading, setLoading] = useState(false);
  const {getFieldDecorator} = props.form;

  return (
    <Form
      onSubmit={e => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
          if (!err) {
            const {username, password} = values;
            // eslint-disable-next-line no-console
            setLoading(true);

            try {
              const token = await login(username, password);
              setLoading(false);
              setToken(token);
              message.success('Login Success', 2);
              setTimeout(() => {
                props.history.replace('/');
              }, 500);
            } catch (error) {
              const {
                response: {data, status},
              }: AxiosError = error;
              setLoading(false);
              if (status === 404) {
                message.error(data.message, 2);
                message.info('Please consider to register', 4);
              } else if (status === 401) {
                message.error(data.message, 2);
              }
            }
          }
        });
      }}>
      <Form.Item>
        {getFieldDecorator('username', {
          rules: [{required: true, message: 'Please input your username!'}],
        })(
          <Input
            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}} />}
            placeholder="Username"
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('password', {
          rules: [{required: true, message: 'Please input your Password!'}],
        })(
          <Input.Password
            prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}} />}
            placeholder="Password"
          />
        )}
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" block loading={loading}>
          Log In
        </Button>
        <Divider orientation="center" />

        <RegisterLinkContainer>
          <Link to="/register">Sign up for an account</Link>
        </RegisterLinkContainer>
      </Form.Item>
    </Form>
  );
};

const LoginForm = Form.create({name: 'login_form'})(LoginFormBase);

export default withRouter(LoginForm);
