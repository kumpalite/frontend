// @flow
// $FlowFixMe
import React, {ReactNode, Fragment} from 'react';
import {Col, Skeleton} from 'antd';
import styled from 'styled-components';

import Card from '../../components/Card';
import UserQuery from '../../components/Query/UserQuery';
import {type GetUser_GetUser} from '../../graphql/types';
import ArticleListLoader from '../../components/ArticleListLoader';

const DashboardContentContainer = styled.div`
  /* margin-top: 2em; */
`;

type Props = {children: (user: GetUser_GetUser) => ReactNode};

const DashboardContent = ({children}: Props) => (
  <DashboardContentContainer>
    <UserQuery query={UserQuery.query}>
      {({data, loading}) => {
        const user = data && data.GetUser ? data.GetUser : null;
        if (loading && user === null)
          return (
            <Fragment>
              <Col span={5} offset={1}>
                <Card>
                  <Skeleton active paragraph={{rows: 2}} />
                </Card>
              </Col>
              <Col span={12}>
                <ArticleListLoader />
              </Col>
              <Col span={5}>
                <Card>
                  <Skeleton active paragraph={{rows: 2}} />
                </Card>
              </Col>
            </Fragment>
          );
        return user ? children(user) : null;
      }}
    </UserQuery>
  </DashboardContentContainer>
);

export default DashboardContent;
