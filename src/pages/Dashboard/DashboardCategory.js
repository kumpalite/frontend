// @flow
import React, {Fragment, useState} from 'react';
import {Select, Divider, Icon} from 'antd';
import CategoryQuery from '../../components/Query/CategoryQuery';
import AddCategoryModal from './AddCategoryModal';
import LoadMoreButton from '../../components/LoadMoreButton';

const {Option} = Select;

type Props = {
  idAuthor: string,
  category: string,
  setCategory: (category: string) => void,
};

const DashboardCategory = ({idAuthor, category, setCategory}: Props) => {
  const [visible, setVisible] = useState(false);

  return (
    <CategoryQuery query={CategoryQuery.query} variables={{idAuthor}}>
      {({loading, data, fetchMore}) => {
        const GetCategoriesByUserId =
          data && data.GetCategoriesByUserId
            ? data.GetCategoriesByUserId
            : null;

        const categories =
          loading && GetCategoriesByUserId === null
            ? []
            : [
                {slugCategory: 'all', name: 'ALL'},
                ...(GetCategoriesByUserId ? GetCategoriesByUserId : []),
              ];

        return (
          <Fragment>
            <AddCategoryModal
              visible={visible}
              idAuthor={idAuthor}
              setVisible={setVisible}
            />
            <Select
              value={category}
              loading={loading}
              defaultValue={'ALL'}
              onChange={e => setCategory(e)}
              style={{width: '100%', margin: '8px 0'}}
              dropdownRender={menu => (
                <>
                  {menu}
                  <Divider style={{margin: '4px 0'}} />
                  <LoadMoreButton
                    loading={loading}
                    onLoadMore={() => {
                      const offset =
                        categories.length > 0 ? categories.length - 1 : 0;
                      fetchMore({
                        query: CategoryQuery.query,
                        variables: {offset, idAuthor},
                        updateQuery: (prev, {fetchMoreResult}) => {
                          if (!fetchMoreResult) return prev;
                          const prevCategory = prev.GetCategoriesByUserId
                            ? prev.GetCategoriesByUserId
                            : [];

                          const nextCategory = fetchMoreResult.GetCategoriesByUserId
                            ? fetchMoreResult.GetCategoriesByUserId
                            : [];

                          return {
                            GetCategoriesByUserId: [
                              ...prevCategory,
                              ...nextCategory,
                            ],
                          };
                        },
                      });
                    }}
                  />
                  <div
                    style={{padding: '8px', cursor: 'pointer'}}
                    onMouseDown={() => {
                      // eslint-disable-next-line no-console
                      console.log('adding');
                      setVisible(true);
                    }}>
                    <Icon type="plus" /> Add item
                  </div>
                </>
              )}>
              {categories.map(val =>
                val ? (
                  <Option key={val.slugCategory} value={val.name}>
                    {val.name}
                  </Option>
                ) : null
              )}
            </Select>
          </Fragment>
        );
      }}
    </CategoryQuery>
  );
};

export default DashboardCategory;
