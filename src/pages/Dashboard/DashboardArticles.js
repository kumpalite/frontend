// @flow
import React, {useState} from 'react';
import {Typography, Divider, List, Button} from 'antd';
import {Link, RouteComponentProps, withRouter} from 'react-router-dom';
import styled from 'styled-components';

import Card from '../../components/Card';
import {timeDifference} from '../../utils/dateConverter';
import ArticlesQuery from '../../components/Query/ArticlesQuery';
import ArticleListLoader from '../../components/ArticleListLoader';
import type {GetAllArticle_GetArticles} from '../../graphql/types';
import LoadMoreButton from '../../components/LoadMoreButton';

const {Text} = Typography;

const DashboardArticlesContainer = styled.div`
  /* margin-top: 4em; */
`;

const StoryCardContainer = styled.div`
  display: flex;
`;

const StoryCardContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const Image = styled.img`
  width: 100px;
  height: 100px;
  margin: 13px 0 0 8px;
  object-fit: cover;
`;

const Title = styled.h2`
  margin: 4px 0;
  a {
    color: black;
    &:hover {
      color: #40a9ff;
    }
  }
`;

type Props = {
  username: string,
  search: string | null,
  category: string | null,
  status: 'DRAFT' | 'PUBLISHED' | null,
} & RouteComponentProps;

const DashboardArticles = ({
  status,
  search,
  username,
  category,
  history,
}: Props) => {
  const [hasNext, setHasNext] = useState(true);
  return (
    <DashboardArticlesContainer>
      <ArticlesQuery
        query={ArticlesQuery.query}
        variables={{status, username, category, keyword: search}}>
        {({data, loading, fetchMore}) => {
          const articles = data && data.GetArticles ? data.GetArticles : null;
          return loading && articles === null ? (
            <ArticleListLoader />
          ) : articles ? (
            <List
              loadMore={
                hasNext && articles.length > 0 ? (
                  <LoadMoreButton
                    loading={loading}
                    onLoadMore={() => {
                      fetchMore({
                        variables: {
                          offset: articles.length,
                        },
                        updateQuery: (prev, {fetchMoreResult}) => {
                          if (!fetchMoreResult) return prev;
                          const prevArticles = prev.GetArticles
                            ? prev.GetArticles
                            : [];

                          const nextArticles = fetchMoreResult.GetArticles
                            ? fetchMoreResult.GetArticles
                            : [];

                          if (nextArticles.length < 5) {
                            setHasNext(false);
                          }

                          return {
                            GetArticles: [...prevArticles, ...nextArticles],
                          };
                        },
                      });
                    }}
                  />
                ) : null
              }
              dataSource={articles}
              renderItem={(article: GetAllArticle_GetArticles) => (
                <Card key={article.id}>
                  <StoryCardContainer>
                    <StoryCardContent>
                      <Title>
                        <Link to={`/story/${article.slug}`}>
                          {article.title}
                        </Link>
                      </Title>
                      <div>
                        <Text type="secondary">
                          Created at: {timeDifference(article.createdAt)}
                        </Text>
                        <Divider type="vertical" />
                        <Text type="secondary">
                          Updated at: {timeDifference(article.updatedAt)}
                        </Text>
                      </div>
                      <Button
                        style={{alignSelf: 'flex-start', marginTop: 'auto'}}
                        onClick={() => history.push(`/write/${article.slug}`)}>
                        Edit
                      </Button>
                    </StoryCardContent>
                    <Image alt={article.title} src={article.thumbnail} />
                  </StoryCardContainer>
                </Card>
              )}
            />
          ) : null;
        }}
      </ArticlesQuery>
    </DashboardArticlesContainer>
  );
};

export default withRouter(DashboardArticles);
