// @flow
import React, {useState} from 'react';
import {Modal, Input, notification, message} from 'antd';
import CreateCategoryMutation from '../../components/Mutation/CreateCategoryMutation';
import CategoryQuery from '../../components/Query/CategoryQuery';

const checkBeforePost = (name: string, slug: string, callback: Function) => {
  if (name.length === 0 || slug.length === 0) {
    notification.open({
      message: 'Oops',
      description: `Name and Slug is needed!`,
    });
    return;
  }

  callback();
};

type Props = {
  idAuthor: string,
  visible: boolean,
  setVisible: (visible: boolean) => void,
};

const AddCategoryModal = ({idAuthor, visible, setVisible}: Props) => {
  const [name, setName] = useState('');
  const [slug, setSlug] = useState('');
  return (
    <CreateCategoryMutation
      mutation={CreateCategoryMutation.mutation}
      onCompleted={() => {
        setName('');
        setSlug('');
        setVisible(false);
      }}
      onError={error => {
        message.error(error.message, 3);
      }}>
      {(create, {loading}) => (
        <Modal
          title="Add New Category"
          visible={visible}
          onOk={() => {
            checkBeforePost(name, slug, () =>
              create({
                variables: {name: name.toUpperCase(), slugCategory: slug},
                update: (proxy, {data}) => {
                  const CreateCategory = data && data.CreateCategory;
                  const local = proxy.readQuery({
                    query: CategoryQuery.query,
                    variables: {idAuthor},
                  });

                  if (local)
                    proxy.writeQuery({
                      query: CategoryQuery.query,
                      variables: {idAuthor},
                      data: {
                        GetCategoriesByUserId: [
                          ...local.GetCategoriesByUserId,
                          CreateCategory,
                        ],
                      },
                    });
                },
              })
            );
          }}
          confirmLoading={loading}
          onCancel={() => {
            setName('');
            setSlug('');
            setVisible(false);
          }}>
          <Input
            value={name}
            placeholder="Input name"
            style={{marginBottom: 18}}
            onChange={e => setName(e.target.value)}
          />
          <Input
            value={slug}
            placeholder="Input slug"
            onChange={e => setSlug(e.target.value)}
          />
        </Modal>
      )}
    </CreateCategoryMutation>
  );
};

export default AddCategoryModal;
