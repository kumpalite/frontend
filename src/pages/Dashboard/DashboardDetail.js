// @flow
import React, {Fragment, useState} from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {Col, Affix, Select, Typography, Input, Button} from 'antd';

import Card from '../../components/Card';
import UserArticles from './DashboardArticles';
import UserDetail from '../Profile/UserDetail';
import DashboardContent from './DashboardContent';
import DashboardCategory from './DashboardCategory';

const {Option} = Select;
const {Text} = Typography;

const DashboardDetail = ({history}: RouteComponentProps) => {
  const [search, setSearch] = useState('');
  const [category, setCategory] = useState('ALL');
  const [status, setStatus] = useState<'ALL' | 'DRAFT' | 'PUBLISHED'>('ALL');

  return (
    <DashboardContent>
      {({id, username}) => (
        <Fragment>
          <Col span={5} offset={1}>
            <Affix offsetTop={70}>
              <UserDetail />
            </Affix>
          </Col>
          <Col span={12}>
            <UserArticles
              username={username}
              search={search === '' ? null : search}
              status={status === 'ALL' ? null : status}
              category={category === 'ALL' ? null : category}
            />
          </Col>
          <Col span={5}>
            <Affix offsetTop={70}>
              <Card>
                <Text>Search</Text>
                <Input.Search
                  onSearch={value => {
                    setSearch(value);
                  }}
                  onChange={e => {
                    e.target.value === '' && setSearch('');
                  }}
                  placeholder="Input search text"
                  style={{width: '100%', margin: '8px 0'}}
                />
                <Text>Status</Text>
                <Select
                  value={status}
                  defaultValue={'ALL'}
                  style={{width: '100%', margin: '8px 0'}}
                  onChange={e => setStatus(e)}>
                  {['ALL', 'DRAFT', 'PUBLISHED'].map(val => (
                    <Option key={val} value={val}>
                      {val}
                    </Option>
                  ))}
                </Select>
                <Text>Category</Text>
                <DashboardCategory
                  idAuthor={id}
                  category={category}
                  setCategory={setCategory}
                />
                <Button
                  type="primary"
                  style={{margin: '8px 0'}}
                  onClick={() => history.push('/write/new')}>
                  Write
                </Button>
              </Card>
            </Affix>
          </Col>
        </Fragment>
      )}
    </DashboardContent>
  );
};

export default withRouter(DashboardDetail);
