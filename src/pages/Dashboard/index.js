// @flow
import React from 'react';
import {Row} from 'antd';
// import {RouteComponentProps} from 'react-router-dom';

import DashboardDetail from './DashboardDetail';

const Profile = () => (
  <Row style={{marginTop: '2em', width: '100%'}} gutter={16}>
    <DashboardDetail />
  </Row>
);

export default Profile;
