// @flow
import React from 'react';
import {Row, Col, Affix} from 'antd';
import {Route} from 'react-router-dom';

import HomeStories from './HomeStories';
import CategoryList from './CategoryList';

const Home = () => (
  <Row style={{marginTop: '2em', width: '100%'}} gutter={16}>
    <Col span={5} offset={1}>
      <Affix offsetTop={70}>
        <Route exact path="/" component={CategoryList} />
        <Route exact path="/category/:category" component={CategoryList} />
      </Affix>
    </Col>
    <Col span={12}>
      <Route exact path="/" component={HomeStories} />
      <Route exact path="/category/:category" component={HomeStories} />
    </Col>
  </Row>
);

export default Home;
