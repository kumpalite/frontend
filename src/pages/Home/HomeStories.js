// @flow
import React, {useState} from 'react';
import styled from 'styled-components';
import {RouteComponentProps} from 'react-router-dom';

import StoryList from '../../components/StoryList';
import ArticlesQuery from '../../components/Query/ArticlesQuery';
import ArticleListLoader from '../../components/ArticleListLoader';

const HomeStoriesContainer = styled.div`
  /* margin-top: 2em; */
`;

const HomeStories = (props: RouteComponentProps) => {
  const {category} = props.match.params;
  const [hasNext, setHasNext] = useState(true);
  return (
    <HomeStoriesContainer>
      <ArticlesQuery
        query={ArticlesQuery.query}
        variables={{category: category ? category.toUpperCase() : null}}>
        {({data, loading, fetchMore}) => {
          const articles = data && data.GetArticles ? data.GetArticles : null;
          return loading && articles === null ? (
            <ArticleListLoader />
          ) : articles ? (
            <>
              <StoryList
                stories={articles}
                loading={loading}
                withLoadMore={hasNext}
                onLoadMore={() =>
                  fetchMore({
                    variables: {
                      offset: articles.length,
                    },
                    updateQuery: (prev, {fetchMoreResult}) => {
                      if (!fetchMoreResult) return prev;
                      const prevArticles = prev.GetArticles
                        ? prev.GetArticles
                        : [];

                      const nextArticles = fetchMoreResult.GetArticles
                        ? fetchMoreResult.GetArticles
                        : [];

                      if (nextArticles.length < 5) {
                        setHasNext(false);
                      }

                      return {
                        GetArticles: [...prevArticles, ...nextArticles],
                      };
                    },
                  })
                }
              />
            </>
          ) : null;
        }}
      </ArticlesQuery>
    </HomeStoriesContainer>
  );
};

export default HomeStories;
