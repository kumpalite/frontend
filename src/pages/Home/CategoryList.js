// @flow
import React from 'react';
import {Typography, List} from 'antd';
import {Link, RouteComponentProps} from 'react-router-dom';

import styled from 'styled-components';
import Card from '../../components/Card';
import getValueFromObject from '../../utils/getValueFromObject';

const {Title} = Typography;

const CategoryListContainer = styled.div`
  /* margin-top: 2em; */
`;

type Props = {
  customLink?: string,
} & RouteComponentProps;

const CategoryList = (props: Props) => {
  const category = getValueFromObject(props, 'match.params.category');
  const lowerCasedCategory = category ? category.toLowerCase() : '';
  return (
    <CategoryListContainer>
      <Card>
        <List
          header={<Title level={4}>Category</Title>}
          dataSource={['News', 'Politik', 'Entertainment', 'Otomotif']}
          renderItem={item => (
            <List.Item>
              <Link
                to={`${
                  props.customLink ? props.customLink : '/category'
                }/${item.toLowerCase()}`}
                style={{
                  color:
                    lowerCasedCategory === item.toLowerCase()
                      ? '#40a9ff'
                      : 'black',
                }}>
                {item}
              </Link>
            </List.Item>
          )}
        />
      </Card>
    </CategoryListContainer>
  );
};

export default CategoryList;
