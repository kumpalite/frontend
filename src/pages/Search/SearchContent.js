// @flow
import React, {useState} from 'react';
import StoryList from '../../components/StoryList';
import ArticlesQuery from '../../components/Query/ArticlesQuery';
import ArticleListLoader from '../../components/ArticleListLoader';

type Props = {
  keyword: string,
};

const SearchContent = ({keyword}: Props) => {
  const [hasNext, setHasNext] = useState(true);
  return (
    <ArticlesQuery query={ArticlesQuery.query} variables={{keyword}}>
      {({data, loading, fetchMore}) => {
        const articles = data && data.GetArticles ? data.GetArticles : null;
        return loading && articles === null ? (
          <ArticleListLoader />
        ) : articles ? (
          <StoryList
            stories={articles}
            loading={loading}
            withLoadMore={hasNext}
            onLoadMore={() =>
              fetchMore({
                variables: {
                  offset: articles.length,
                },
                updateQuery: (prev, {fetchMoreResult}) => {
                  if (!fetchMoreResult) return prev;
                  const prevArticles = prev.GetArticles ? prev.GetArticles : [];

                  const nextArticles = fetchMoreResult.GetArticles
                    ? fetchMoreResult.GetArticles
                    : [];

                  if (nextArticles.length < 5) {
                    setHasNext(false);
                  }

                  return {
                    GetArticles: [...prevArticles, ...nextArticles],
                  };
                },
              })
            }
          />
        ) : null;
      }}
    </ArticlesQuery>
  );
};

export default SearchContent;
