// @flow
import React from 'react';
import styled from 'styled-components';
import {RouteComponentProps} from 'react-router-dom';
import SearchContent from './SearchContent';
import {Divider, Row, Col, Typography} from 'antd';

const TitleContainer = styled.div`
  margin: 8px 8px 0;
`;

const Search = ({match}: RouteComponentProps) => (
  <Row style={{marginTop: '2em', width: '100%'}}>
    <Col span={12} offset={6}>
      <TitleContainer>
        <Typography.Title level={3}>
          Hasil pencarian &quot;{match.params.search}&quot;
        </Typography.Title>
      </TitleContainer>
      <Divider />
      <SearchContent keyword={match.params.search} />
    </Col>
  </Row>
);

export default Search;
