// @flow
// $FlowFixMe
import React, {ReactNode} from 'react';
import ArticleQuery from '../../components/Query/ArticleQuery';
import {type GetArticle_GetArticleBySlug} from '../../graphql/types';
import {Skeleton} from 'antd';

type Props = {
  slug: string,
  children: (data: GetArticle_GetArticleBySlug) => ReactNode,
};

const EditContainer = ({children, slug}: Props) => (
  <ArticleQuery query={ArticleQuery.query} variables={{slug}}>
    {({data, loading}) => {
      const article =
        data && data.GetArticleBySlug ? data.GetArticleBySlug : null;
      return loading && article === null ? (
        <Skeleton active paragraph={{rows: 3}} />
      ) : article ? (
        children(article)
      ) : null;
    }}
  </ArticleQuery>
);

export default EditContainer;
