// @flow
import React from 'react';
import {Button} from 'antd';
import {withRouter} from 'react-router-dom';

import {type SubmitButtonType} from './writeTypes';
import ArticlesQuery from '../../components/Query/ArticlesQuery';
import CreateArticleMutation from '../../components/Mutation/CreateArticleMutation';
import UserQuery from '../../components/Query/UserQuery';

const SaveContainer = ({
  slug,
  title,
  value,
  status,
  history,
  category,
  children,
}: SubmitButtonType) => (
  <UserQuery query={UserQuery.query}>
    {({data, loading}) => {
      const user = data && data.GetUser ? data.GetUser : null;
      return loading && user === null ? (
        <Button loading />
      ) : user ? (
        <CreateArticleMutation
          mutation={CreateArticleMutation.mutation}
          onCompleted={() => history.goBack()}>
          {(createArticle, {loading}) =>
            children({
              loading,
              submit: () => {
                const now = new Date();
                createArticle({
                  variables: {
                    slug,
                    title,
                    status,
                    category,
                    content: JSON.stringify(value.toJSON()),
                  },
                  optimisticResponse: {
                    __typename: 'Article',
                    CreateArticle: {
                      __typename: 'Article',
                      id: 'ulalalala',
                      title,
                      content: JSON.stringify(value.toJSON()),
                      slug: title.replace(' ', '-'),
                      createdAt: now.toISOString(),
                      updatedAt: now.toISOString(),
                      author: {
                        __typename: 'User',
                        id: user.id,
                        fullname: user.fullname,
                        username: user.username,
                      },
                      thumbnail:
                        'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1564901467/imchrsgn7fyt2i8pxsmb.jpg',
                      totalComment: 0,
                    },
                  },
                  update: (proxy, {data}) => {
                    const CreateArticle =
                      data && data.CreateArticle ? data.CreateArticle : null;
                    const local = proxy.readQuery({
                      query: ArticlesQuery.query,
                      variables: {
                        status: null,
                        username: user.username,
                        category: null,
                        keyword: null,
                      },
                    });

                    if (local && createArticle) {
                      proxy.writeQuery({
                        query: ArticlesQuery.query,
                        variables: {
                          status: null,
                          username: user.username,
                          category: null,
                          keyword: null,
                        },
                        data: {
                          GetArticles: [CreateArticle, ...local.GetArticles],
                        },
                      });
                    }
                  },
                });
              },
            })
          }
        </CreateArticleMutation>
      ) : null;
    }}
  </UserQuery>
);

export default withRouter(SaveContainer);
