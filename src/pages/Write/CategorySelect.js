// @flow
import React from 'react';
import {Select} from 'antd';
import styled from 'styled-components';
import UserQuery from '../../components/Query/UserQuery';
import CategoryQuery from '../../components/Query/CategoryQuery';
import LoadMoreButton from '../../components/LoadMoreButton';

const CategorySelectContainer = styled.div`
  margin: 0 0 10px 0;

  .ant-select-selection {
    border: none;
    outline: none;
    box-shadow: none;
  }
`;

type Props = {
  values: string[],
  handleChange: (selectedItems: string[]) => void,
};

const CategorySelect = ({values, handleChange}: Props) => (
  <UserQuery query={UserQuery.query}>
    {({data, loading}) => {
      const user = data && data.GetUser ? data.GetUser : null;
      return loading && user === null ? (
        <div>Loading...</div>
      ) : user ? (
        <CategoryQuery
          query={CategoryQuery.query}
          variables={{idAuthor: user.id}}>
          {({loading, data, fetchMore}) => {
            const categories =
              data && data.GetCategoriesByUserId
                ? data.GetCategoriesByUserId
                : null;

            if (loading && categories === null) return <div>Loading...</div>;

            if (categories) {
              const categoriesName = categories.map(val => val && val.name);
              const filteredOptions = categoriesName.filter(
                o => !values.includes(o)
              );

              return (
                <CategorySelectContainer>
                  <Select
                    value={values}
                    mode="multiple"
                    loading={loading}
                    style={{width: '100%'}}
                    onChange={handleChange}
                    placeholder="Select categories"
                    dropdownRender={menu => (
                      <>
                        {menu}
                        <div style={{margin: '8px 0'}}>
                          <LoadMoreButton
                            loading={loading}
                            onLoadMore={() => {
                              fetchMore({
                                variables: {
                                  offset: categories.length,
                                },
                                updateQuery: (prev, {fetchMoreResult}) => {
                                  if (!fetchMoreResult) return prev;
                                  const prevCategory = prev.GetCategoriesByUserId
                                    ? prev.GetCategoriesByUserId
                                    : [];

                                  const nextCategory = fetchMoreResult.GetCategoriesByUserId
                                    ? fetchMoreResult.GetCategoriesByUserId
                                    : [];

                                  return {
                                    GetCategoriesByUserId: [
                                      ...prevCategory,
                                      ...nextCategory,
                                    ],
                                  };
                                },
                              });
                            }}
                          />
                        </div>
                      </>
                    )}>
                    {filteredOptions.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </CategorySelectContainer>
              );
            }
            return null;
          }}
        </CategoryQuery>
      ) : null;
    }}
  </UserQuery>
);

export default CategorySelect;
