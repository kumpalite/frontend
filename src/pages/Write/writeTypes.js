// @flow
import {Value} from 'slate';
// $FlowFixMe
import {ReactNode} from 'react';
import {RouteComponentProps} from 'react-router-dom';

type SubmitDataType = {
  slug: string,
  value: Value,
  title: string,
  status: string,
  category: Array<?string>,
};

type ComponentType = {
  loading: boolean,
  submit: () => void,
};

export type SubmitButtonType = {
  children: (data: ComponentType) => ReactNode,
} & SubmitDataType &
  RouteComponentProps;
