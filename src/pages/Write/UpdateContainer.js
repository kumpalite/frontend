// @flow
import React from 'react';
import {withRouter} from 'react-router-dom';

import {type SubmitButtonType} from './writeTypes';
// import ArticlesQuery from '../../components/Query/ArticlesQuery';
import UpdateArticleMutation from '../../components/Mutation/UpdateArticleMutation';

const UpdateContainer = ({
  id,
  slug,
  title,
  value,
  status,
  history,
  category,
  children,
}: {id: string} & SubmitButtonType) => (
  <UpdateArticleMutation
    mutation={UpdateArticleMutation.mutation}
    onCompleted={() => history.goBack()}>
    {(updateArticle, {loading}) =>
      children({
        loading,
        submit: () => {
          updateArticle({
            variables: {
              id,
              slug,
              title,
              status,
              category,
              content: JSON.stringify(value.toJSON()),
            },
          });
        },
      })
    }
  </UpdateArticleMutation>
);

export default withRouter(UpdateContainer);
