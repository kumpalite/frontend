// @flow
import React, {Fragment, useState} from 'react';
import {Value} from 'slate';
import Plain from 'slate-plain-serializer';
import {Button, notification, Select} from 'antd';
import {withRouter, RouteComponentProps} from 'react-router-dom';

import SaveContainer from './SaveContainer';
import UpdateContainer from './UpdateContainer';

const {Option} = Select;

type Props = {
  id: string | null,
  slug: string,
  title: string,
  value: Value,
  category: string[],
} & RouteComponentProps;

const showNotification = (message: string) =>
  notification.open({
    message: 'Oops',
    description: message,
  });

const checkBeforePost = (
  title: string,
  content: string,
  callback: Function
) => {
  const titleMin = 10,
    titleMax = 125,
    contentMin = 200,
    contentMax = 999;

  if (title.length < titleMin || title.length > titleMax) {
    showNotification(
      `Title should be ${titleMin} to ${titleMax} character length`
    );
    return;
  }

  if (content.length < contentMin || content.length > contentMax) {
    showNotification(
      `Content should be ${contentMin} to ${contentMax} character length`
    );
    return;
  }

  callback();
};

const STATUS = ['DRAFT', 'PUBLISHED'];

const SubmitButtons = ({id, title, value, category, slug}: Props) => {
  const content = value ? Plain.serialize(value) : '';
  const [status, setStatus] = useState('DRAFT');

  return (
    <Fragment>
      <Select
        value={status}
        defaultValue="DRAFT"
        style={{width: 125}}
        onChange={e => setStatus(e)}>
        {STATUS.map(val => (
          <Option key={val} value={val}>
            {val}
          </Option>
        ))}
      </Select>
      {id ? (
        <UpdateContainer
          id={id}
          slug={slug}
          title={title}
          value={value}
          status={status}
          category={category}>
          {({loading, submit}) => (
            <Button
              loading={loading}
              style={{marginLeft: 8}}
              onClick={() => {
                checkBeforePost(title, content, () => submit());
              }}>
              UPDATE
            </Button>
          )}
        </UpdateContainer>
      ) : (
        <SaveContainer
          slug={slug}
          title={title}
          value={value}
          status={status}
          category={category}>
          {({loading, submit}) => (
            <Button
              loading={loading}
              style={{marginLeft: 8}}
              onClick={() => {
                checkBeforePost(title, content, () => submit());
              }}>
              SAVE
            </Button>
          )}
        </SaveContainer>
      )}
    </Fragment>
  );
};

export default withRouter(SubmitButtons);
