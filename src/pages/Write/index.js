// @flow
import React, {Suspense} from 'react';
import {Value} from 'slate';
import styled from 'styled-components';
import {RouteComponentProps} from 'react-router-dom';
import {Layout, Row, Col, Divider, Skeleton} from 'antd';

import SubmitButtons from './SubmitButtons';
import CategorySelect from './CategorySelect';
import EditContainer from './EditContainer';
import getValueFromObject from '../../utils/getValueFromObject';
const Editor = React.lazy(() => import('../../components/Editor'));
const {Content} = Layout;

const EditorContainer = styled.div`
  color: black;
  padding: 40px;
  max-width: 750px;
  border-radius: 4.5px;
  margin: 2em auto 45px;
  background-color: white;
  box-shadow: rgba(118, 143, 255, 0.1) 0px 16px 24px 0px;
`;

const BaseInput = styled.input`
  width: 100%;
  border: none;
  outline: none;
  padding: 8px 0;
  margin-bottom: 8px;
  font-family: inherit;
`;

const TitleInput = styled(BaseInput)`
  width: 100%;
  padding: 8px;
  font-size: 32px;
  font-weight: bold;
`;

const InputContainer = styled.div`
  padding: 8px;
  display: flex;
  align-items: center;
`;

const SlugInput = styled(BaseInput)`
  margin: 0 16px 0;
  font-size: 16px;
`;

type WriteData = {
  data: {
    id: string,
    title: string,
    slug: string,
    value: Value,
    category: string[],
  } | null,
};

const WriteContent = (props: WriteData) => {
  const [title, setTitle] = React.useState('');
  const [slug, setSlug] = React.useState('');
  const [category, setCategory] = React.useState<string[]>([]);
  const [value, setValue] = React.useState<Value>(null);

  React.useEffect(() => {
    if (props.data) {
      const {slug, title, value, category} = props.data;
      setSlug(slug);
      setTitle(title);
      setValue(value);
      setCategory(category);
    }
  }, []);

  return (
    <Layout>
      <Content>
        <Row>
          <Col span={12} offset={6}>
            <EditorContainer>
              <Suspense fallback={<Skeleton active paragraph={{rows: 3}} />}>
                <TitleInput
                  value={title}
                  placeholder="Write title here"
                  onChange={e => setTitle(e.target.value)}
                />
                <InputContainer>
                  Slug:
                  <SlugInput
                    value={slug}
                    placeholder="Write slug here"
                    onChange={e => setSlug(e.target.value)}
                  />
                </InputContainer>

                <Editor
                  value={value}
                  onEditorValueChange={val => setValue(val)}
                  isEnableEditing={true}
                />

                <Divider style={{margin: '16px 0'}} />
                <div style={{margin: '8px'}}>Category:</div>
                <CategorySelect
                  values={category}
                  handleChange={items => setCategory(items)}
                />
                <Divider style={{margin: '16px 0'}} />
                <SubmitButtons
                  id={getValueFromObject(props, 'data.id')}
                  slug={slug}
                  title={title}
                  value={value}
                  category={category}
                />
              </Suspense>
            </EditorContainer>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

const Write = (props: RouteComponentProps) => {
  const {slug} = props.match.params;
  return slug === 'new' ? (
    <WriteContent data={null} />
  ) : (
    <EditContainer slug={slug}>
      {({id, title, content, slug, category}) => {
        const cat: string[] = (category: any);

        return (
          <WriteContent
            data={{
              id,
              slug,
              title,
              value: Value.fromJSON(JSON.parse(content)),
              category: cat.filter(val => val !== ''),
            }}
          />
        );
      }}
    </EditContainer>
  );
};

export default Write;
