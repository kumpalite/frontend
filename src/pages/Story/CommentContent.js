// @flow
import React, {useState, Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Typography, Comment, List, notification, Divider} from 'antd';
import CommentEditor from '../../components/CommentEditor';
import CreateCommentMutation from '../../components/Mutation/CreateCommentMutation';
import ArticleQuery from '../../components/Query/ArticleQuery';
import useToken from '../../hooks/useToken';
import CommentsQuery from '../../components/Query/CommentsQuery';
import LoadMoreButton from '../../components/LoadMoreButton';

const {Title} = Typography;

const checkBeforePost = (content: string, callback: Function) => {
  if (content.length === 0) {
    notification.open({
      message: 'Oops',
      description: `Write something please!`,
    });
    return;
  }

  callback();
};

type Props = {
  slug: string,
  idArticle: string,
  totalComment: number,
};

const CommentContent = ({slug, idArticle, totalComment}: Props) => {
  const {token} = useToken();
  const [comment, setComment] = useState('');
  const [hasNext, setHasNext] = useState(true);
  return (
    <CommentsQuery query={CommentsQuery.query} variables={{slug}}>
      {({data, loading: queryLoading, fetchMore}) => {
        const comments =
          data && data.GetCommentByArticleSlug
            ? data.GetCommentByArticleSlug
            : null;

        return queryLoading && comments === null ? (
          <div>loading</div>
        ) : comments ? (
          <CreateCommentMutation mutation={CreateCommentMutation.mutation}>
            {(createComment, {loading}) => (
              <List
                header={
                  <>
                    <Title level={4}>{totalComment} Comments</Title>
                    <CommentEditor
                      value={comment}
                      disabled={token === null}
                      onChange={e => setComment(e.target.value)}
                      onSubmit={() => {
                        // eslint-disable-next-line no-console
                        checkBeforePost(comment, () =>
                          createComment({
                            variables: {
                              idArticle,
                              content: comment,
                            },
                            optimisticResponse: {
                              __typename: 'Comment',
                              CreateComment: {
                                __typename: 'Comment',
                                id: 'ulalalala',
                                content: comment,
                                author: {
                                  __typename: 'User',
                                  id: 'uuuyeee',
                                  fullname: 'Loading',
                                  username: 'Loading',
                                },
                              },
                            },
                            update: (proxy, {data}) => {
                              const CreateComment = data
                                ? data.CreateComment
                                : null;

                              const localArticle = proxy.readQuery({
                                query: ArticleQuery.query,
                                variables: {slug},
                              });

                              const localComments = proxy.readQuery({
                                query: CommentsQuery.query,
                                variables: {slug},
                              });

                              if (localArticle && CreateComment)
                                proxy.writeQuery({
                                  query: ArticleQuery.query,
                                  variables: {slug},
                                  data: {
                                    GetArticleBySlug: {
                                      ...localArticle.GetArticleBySlug,
                                      totalComment:
                                        localArticle.GetArticleBySlug
                                          .totalComment + 1,
                                    },
                                  },
                                });

                              if (localComments && CreateComment) {
                                proxy.writeQuery({
                                  query: CommentsQuery.query,
                                  variables: {slug},
                                  data: {
                                    GetCommentByArticleSlug: [
                                      CreateComment,
                                      ...localComments.GetCommentByArticleSlug,
                                    ],
                                  },
                                });
                              }

                              setComment('');
                            },
                          })
                        );
                      }}
                      submitting={loading}
                    />
                  </>
                }
                itemLayout="horizontal"
                dataSource={comments}
                renderItem={item => (
                  <Fragment>
                    <Comment
                      author={
                        <Link
                          style={{color: 'grey'}}
                          to={`/profile/${item.author.username}`}>
                          {item.author.fullname}
                        </Link>
                      }
                      content={<p>{item.content}</p>}
                    />
                    <Divider style={{margin: 0}} />
                  </Fragment>
                )}
                loadMore={
                  hasNext ? (
                    <LoadMoreButton
                      loading={queryLoading}
                      onLoadMore={() => {
                        fetchMore({
                          variables: {
                            offset: comments.length,
                          },
                          updateQuery: (prev, {fetchMoreResult}) => {
                            if (!fetchMoreResult) return prev;
                            const prevComments = prev.GetCommentByArticleSlug
                              ? prev.GetCommentByArticleSlug
                              : [];

                            const nextComments = fetchMoreResult.GetCommentByArticleSlug
                              ? fetchMoreResult.GetCommentByArticleSlug
                              : [];

                            if (nextComments.length < 5) {
                              setHasNext(false);
                            }

                            return {
                              GetCommentByArticleSlug: [
                                ...prevComments,
                                ...nextComments,
                              ],
                            };
                          },
                        });
                      }}
                    />
                  ) : null
                }
              />
            )}
          </CreateCommentMutation>
        ) : null;
      }}
    </CommentsQuery>
  );
};

export default CommentContent;
