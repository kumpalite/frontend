// @flow
import React, {Fragment} from 'react';
import {Value} from 'slate';
import styled from 'styled-components';
import Plain from 'slate-plain-serializer';
import {Skeleton, Tag, Typography, Divider} from 'antd';
import {Link, withRouter, RouteComponentProps} from 'react-router-dom';

import Meta from '../../components/Meta';
import ArticleQuery from '../../components/Query/ArticleQuery';
import {convertToDateString} from '../../utils/dateConverter';
import CommentContent from './CommentContent';
const Editor = React.lazy(() => import('../../components/Editor'));

const {Title, Text} = Typography;

const TagContainer = styled.div`
  margin-bottom: 16px;
  display: inline-block;
`;

const EditorContainer = styled.div`
  margin: 4px;
  color: black;
  padding: 40px;
  max-width: 750px;
  border-radius: 4.5px;
  background-color: white;
  box-shadow: rgba(118, 143, 255, 0.1) 0px 16px 24px 0px;
`;

type Props = {} & RouteComponentProps;

const getContent = (content: string) => {
  try {
    const valObj = JSON.parse(content);
    return Value.fromJSON(valObj);
  } catch (error) {
    return Plain.deserialize(content);
  }
};

const articleContent = ({match}: Props) => (
  <ArticleQuery
    query={ArticleQuery.query}
    variables={{slug: match.params.slug}}>
    {({data, loading, error}) => {
      const article =
        data && data.GetArticleBySlug ? data.GetArticleBySlug : null;
      if (error) return <div>{error.message}</div>;
      return loading && article === null ? (
        <EditorContainer>
          <Skeleton active paragraph={{rows: 10}} />
        </EditorContainer>
      ) : article ? (
        <Fragment>
          <EditorContainer>
            <React.Suspense
              fallback={<Skeleton active paragraph={{rows: 10}} />}>
              <Meta
                title={article.title}
                description={Plain.serialize(getContent(article.content))}
              />
              <Title level={2}>{article.title}</Title>
              <Divider orientation="left">
                <Text>
                  By{' '}
                  <Text style={{color: '#40a9ff'}}>
                    <Link
                      to={`/profile/${
                        article.author ? article.author.username : ''
                      }`}>
                      {article.author ? article.author.fullname : ''}
                    </Link>
                  </Text>
                </Text>
                <Divider type="vertical" />
                <Text style={{color: '#999999', alignSelf: 'right'}}>
                  {convertToDateString(article.createdAt)}
                </Text>
              </Divider>

              {article.category.map(
                category =>
                  category !== '' && (
                    <TagContainer key={category}>
                      <Tag
                        closable={false}
                        style={{border: 'none', color: '#69c0ff'}}>
                        {category}
                      </Tag>
                    </TagContainer>
                  )
              )}
              <Editor
                isEnableEditing={false}
                value={getContent(article.content)}
              />
            </React.Suspense>
          </EditorContainer>
          <EditorContainer style={{marginTop: 40}}>
            <CommentContent
              slug={article.slug}
              idArticle={article.id}
              totalComment={article.totalComment}
            />
          </EditorContainer>
        </Fragment>
      ) : null;
    }}
  </ArticleQuery>
);

export default withRouter(articleContent);
