// @flow
import React from 'react';
import styled from 'styled-components';
import StoryList from '../../components/StoryList';
import RelatedArticlesQuery from '../../components/Query/RelatedArticlesQuery';
import ArticleListLoader from '../../components/ArticleListLoader';
import {Typography, Divider} from 'antd';

const RelatedContentContainer = styled.div`
  margin-top: 4em;
`;

const RelatedContent = ({slug}: {slug: string}) => (
  <RelatedContentContainer>
    <Divider orientation="left">
      <Typography.Title level={3}>Related Article</Typography.Title>
    </Divider>
    <RelatedArticlesQuery query={RelatedArticlesQuery.query} variables={{slug}}>
      {({data, loading}) => {
        const relatedArticles =
          data && data.GetRelatedArticles ? data.GetRelatedArticles : null;
        return loading && relatedArticles === null ? (
          <ArticleListLoader />
        ) : relatedArticles ? (
          <StoryList stories={relatedArticles} loading={loading} />
        ) : null;
      }}
    </RelatedArticlesQuery>
  </RelatedContentContainer>
);

export default RelatedContent;
