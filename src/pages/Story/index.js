// @flow
import React from 'react';
import {Row, Col, Affix} from 'antd';
import {RouteComponentProps} from 'react-router-dom';

import StoryContent from './StoryContent';
import RelatedContent from './RelatedContent';
import CategoryList from '../Home/CategoryList';

const Story = (props: RouteComponentProps) => {
  const [story, setStory] = React.useState({title: '', content: null});

  React.useEffect(() => {
    window.scrollTo({behavior: 'smooth', top: 0});
    const localDraft = localStorage.getItem('draft');
    if (localDraft) setStory({title: 'ulala', content: localDraft});
  }, [props.location.pathname]);

  return (
    <Row style={{marginTop: '2em', width: '100%'}} gutter={16}>
      <Col span={5} offset={1}>
        <Affix offsetTop={70}>
          <CategoryList />
        </Affix>
      </Col>
      <Col span={12}>
        <StoryContent id="1" story={story} />
        <RelatedContent slug={props.match.params.slug} />
      </Col>
    </Row>
  );
};

export default Story;
