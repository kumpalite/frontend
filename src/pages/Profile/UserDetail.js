// @flow
import React, {Fragment} from 'react';

import styled from 'styled-components';
import Card from '../../components/Card';
import UserQuery from '../../components/Query/UserQuery';
import {Typography} from 'antd';
import Meta from '../../components/Meta';

const {Text, Title} = Typography;

const UserDetailContainer = styled.div`
  /* margin-top: 2em; */
`;

type Props = {username?: string};

const UserDetail = ({username}: Props) => (
  <UserDetailContainer>
    <Card>
      <UserQuery query={UserQuery.query} variables={{username}}>
        {({data, loading}) => {
          const user = data && data.GetUser ? data.GetUser : null;
          return loading && user === null ? (
            <div>Loading...</div>
          ) : user ? (
            <Fragment>
              <Meta
                title={user.fullname}
                description={`${user.fullname} Profile`}
              />
              <Title>{user.fullname}</Title>
              <Text>@{user.username}</Text>
            </Fragment>
          ) : null;
        }}
      </UserQuery>
    </Card>
  </UserDetailContainer>
);

export default UserDetail;
