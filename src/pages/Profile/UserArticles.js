// @flow
import React, {useState} from 'react';
import styled from 'styled-components';
import {RouteComponentProps} from 'react-router-dom';
import StoryList from '../../components/StoryList';
import ArticlesQuery from '../../components/Query/ArticlesQuery';
import ArticleListLoader from '../../components/ArticleListLoader';

const UserArticlesContainer = styled.div`
  /* margin-top: 4em; */
`;

type Props = {} & RouteComponentProps;

const UserArticles = ({match}: Props) => {
  const {username, category} = match.params;
  const [hasNext, setHasNext] = useState(true);
  return (
    <UserArticlesContainer>
      <ArticlesQuery
        query={ArticlesQuery.query}
        variables={{
          username,
          category: category ? category.toUpperCase() : null,
        }}>
        {({data, loading, fetchMore}) => {
          const articles = data && data.GetArticles ? data.GetArticles : null;
          return loading && articles === null ? (
            <ArticleListLoader />
          ) : articles ? (
            <StoryList
              stories={articles}
              loading={loading}
              withLoadMore={hasNext}
              onLoadMore={() =>
                fetchMore({
                  variables: {
                    offset: articles.length,
                  },
                  updateQuery: (prev, {fetchMoreResult}) => {
                    if (!fetchMoreResult) return prev;
                    const prevArticles = prev.GetArticles
                      ? prev.GetArticles
                      : [];

                    const nextArticles = fetchMoreResult.GetArticles
                      ? fetchMoreResult.GetArticles
                      : [];

                    if (nextArticles.length < 5) {
                      setHasNext(false);
                    }

                    return {
                      GetArticles: [...prevArticles, ...nextArticles],
                    };
                  },
                })
              }
            />
          ) : null;
        }}
      </ArticlesQuery>
    </UserArticlesContainer>
  );
};

export default UserArticles;
