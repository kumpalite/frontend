// @flow
import React from 'react';
import {Route, RouteComponentProps} from 'react-router-dom';
import UserArticles from './UserArticles';
import {Row, Col, Affix} from 'antd';
import UserDetail from './UserDetail';
import UserCategory from './UserCategory';

const Profile = ({match}: RouteComponentProps) => {
  const {username} = match.params;
  return (
    <Row style={{marginTop: '2em', width: '100%'}} gutter={16}>
      <Col span={5} offset={1}>
        <Affix offsetTop={70}>
          <UserDetail username={username} />
        </Affix>
      </Col>
      <Col span={12}>
        <Route exact component={UserArticles} path="/profile/:username" />
        <Route
          exact
          component={UserArticles}
          path="/profile/:username/:category"
        />
      </Col>
      <Col span={5}>
        <Affix offsetTop={70}>
          <Route
            exact
            path="/profile/:username"
            render={props => (
              <UserCategory {...props} customLink={`/profile/${username}`} />
            )}
          />
          <Route
            exact
            path="/profile/:username/:category"
            render={props => (
              <UserCategory {...props} customLink={`/profile/${username}`} />
            )}
          />
        </Affix>
      </Col>
    </Row>
  );
};

export default Profile;
