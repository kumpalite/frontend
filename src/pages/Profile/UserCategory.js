// @flow
import React, {useState} from 'react';
import {Typography, List, Skeleton} from 'antd';
import {Link, RouteComponentProps} from 'react-router-dom';

import styled from 'styled-components';
import Card from '../../components/Card';
import getValueFromObject from '../../utils/getValueFromObject';
import UserQuery from '../../components/Query/UserQuery';
import CategoryQuery from '../../components/Query/CategoryQuery';
import LoadMoreButton from '../../components/LoadMoreButton';

const {Title} = Typography;

const UserCategoryContainer = styled.div`
  /* margin-top: 2em; */
`;

type Props = {
  customLink?: string,
} & RouteComponentProps;

const UserCategory = (props: Props) => {
  const category = getValueFromObject(props, 'match.params.category');
  const username = getValueFromObject(props, 'match.params.username');
  const lowerCasedCategory = category ? category.toLowerCase() : '';
  const [hasNext, setHasNext] = useState(true);
  return (
    <UserCategoryContainer>
      <Card style={{maxHeight: 'calc(100vh / 2)', overflow: 'scroll'}}>
        <UserQuery query={UserQuery.query} variables={{username}}>
          {({data, loading}) => {
            const user = data && data.GetUser ? data.GetUser : null;
            return loading && user === null ? (
              <Skeleton active paragraph={{rows: 2}} />
            ) : user ? (
              <CategoryQuery
                query={CategoryQuery.query}
                variables={{idAuthor: user.id}}>
                {({data, loading, fetchMore}) => {
                  const categories =
                    data && data.GetCategoriesByUserId
                      ? data.GetCategoriesByUserId
                      : null;

                  return loading && categories === null ? (
                    <Skeleton active paragraph={{rows: 2}} />
                  ) : categories ? (
                    <List
                      header={<Title level={4}>Category</Title>}
                      dataSource={categories.map(val => val && val.name)}
                      renderItem={item => (
                        <List.Item>
                          <Link
                            to={`${
                              props.customLink ? props.customLink : '/category'
                            }/${item.toLowerCase()}`}
                            style={{
                              color:
                                lowerCasedCategory === item.toLowerCase()
                                  ? '#40a9ff'
                                  : 'black',
                            }}>
                            {item}
                          </Link>
                        </List.Item>
                      )}
                      loadMore={
                        hasNext ? (
                          <LoadMoreButton
                            loading={loading}
                            onLoadMore={() =>
                              fetchMore({
                                variables: {
                                  offset: categories.length,
                                },
                                updateQuery: (prev, {fetchMoreResult}) => {
                                  if (!fetchMoreResult) return prev;
                                  const prevCategory = prev.GetCategoriesByUserId
                                    ? prev.GetCategoriesByUserId
                                    : [];

                                  const nextCategory = fetchMoreResult.GetCategoriesByUserId
                                    ? fetchMoreResult.GetCategoriesByUserId
                                    : [];

                                  if (nextCategory.length < 5) {
                                    setHasNext(false);
                                  }

                                  return {
                                    GetCategoriesByUserId: [
                                      ...prevCategory,
                                      ...nextCategory,
                                    ],
                                  };
                                },
                              })
                            }
                          />
                        ) : null
                      }
                    />
                  ) : null;
                }}
              </CategoryQuery>
            ) : null;
          }}
        </UserQuery>
      </Card>
    </UserCategoryContainer>
  );
};

export default UserCategory;
