// @flow
import React, {useState} from 'react';
import {FormComponentProps} from 'antd/lib/form';
import {Form, Input, Icon, Button, message} from 'antd';
import {RouteComponentProps, withRouter} from 'react-router';
import {AxiosError} from 'axios';

import {register} from '../../services/authService';
import useToken from '../../hooks/useToken';

type Props = {} & FormComponentProps & RouteComponentProps;

const RegisterFormBase = (props: Props) => {
  const {setToken} = useToken();
  const [loading, setLoading] = useState(false);
  const {getFieldDecorator} = props.form;

  return (
    <Form
      onSubmit={e => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
          if (!err) {
            setLoading(true);
            try {
              const token = await register(values);
              setLoading(false);
              setToken(token);
              message.success('Register Success', 2);
              setTimeout(() => {
                props.history.replace(`/profile/${values.username}`);
              }, 500);
            } catch (error) {
              const {
                response: {data},
              }: AxiosError = error;
              setLoading(false);
              message.error(data.message, 2);
            }
          }
        });
      }}>
      <Form.Item>
        {getFieldDecorator('username', {
          rules: [{required: true, message: 'Please input your Username!'}],
        })(
          <Input
            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}} />}
            placeholder="Username"
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('fullname', {
          rules: [{required: true, message: 'Please input your Full Name!'}],
        })(
          <Input
            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}} />}
            placeholder="Full Name"
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('email', {
          rules: [
            {required: true, message: 'Please input your Email!'},
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
          ],
        })(
          <Input
            prefix={<Icon type="mail" style={{color: 'rgba(0,0,0,.25)'}} />}
            placeholder="Email"
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('password', {
          rules: [
            {required: true, message: 'Please input your Password!'},
            {min: 6, message: 'Password must be 6 or more character!'},
          ],
        })(
          <Input.Password
            prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}} />}
            placeholder="Password"
          />
        )}
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" block loading={loading}>
          Register
        </Button>
      </Form.Item>
    </Form>
  );
};

const RegisterForm = Form.create({name: 'register_form'})(RegisterFormBase);

export default withRouter(RegisterForm);
